{
  # from https://github.com/jakeisnt/react-turn-based-game/blob/1b5f333fe308ad2b90fdf5a5aa6fd9761d774297/flake.nix
  # https://github.com/soywod/react-pin-field/blob/master/flake.nix
  
  description = "DataWars2 Frontend";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.05";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, ... }: utils.lib.eachDefaultSystem (system: 
    let
    
      pkgs = import nixpkgs {
        inherit system;
        overlays = [  ];
      };
      
      package_name = "dw2_frontend";
      # name in teh package.json
      package_name_alt = "gw2_stats";
      
      nodejs = pkgs.pkgs.nodejs-16_x;

    in rec {

      # somehow this worked
      # https://discourse.nixos.org/t/how-to-use-nix-to-build-a-create-react-app-project/5200/10
      packages."${package_name}" = pkgs.mkYarnPackage {
            name = "${package_name}";
            buildInputs = [
              nodejs
            ];
            src = ./.;
            packageJSON = "${./package.json}";
            yarnLock = "${./yarn.lock}";
            
            # this does not seem to work
            doDist = false;
            
            buildPhase = ''
              runHook preBuild
              shopt -s dotglob

              rm deps/${package_name_alt}/node_modules
              mkdir deps/${package_name_alt}/node_modules
              
              pushd deps/${package_name_alt}/node_modules
                ln -s ../../../node_modules/* .
              popd
              
              cd deps/${package_name_alt}
              
              yarn --offline build
              
              cd ../../
              
              runHook postBuild
            '';
            installPhase = ''
              runHook preInstall

              mv deps/${package_name_alt}/build $out

              runHook postInstall
            '';
            
            # I just dont want it to run
            distPhase = ''
            
            '';
          };

      defaultPackage = packages."${package_name}";
      
    });
}